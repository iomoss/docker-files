# cura-ati

Repository for running `cura-15.04.5` on any linux.

## Building / running the image
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/cura-ati
$ ./run.sh
```
Or stand-alone;
```bash
$ docker run \
    --privileged \
    -it \
	-v /tmp/.X11-unix:/tmp/.X11-unix:rw
    -v /tmp/.docker.xauth:/tmp/.docker.xauth:rw
	-e DISPLAY=$DISPLAY \
	-e XAUTHORITY=/tmp/.docker.xauth
    -v ~/:/home/$USER/ \
    iomoss/cura-ati
```

This will;

1. Install the program in a docker image.
2. Run the docker image, and start the cura program on hosts x-server.
3. The host system is mounted under /host/
