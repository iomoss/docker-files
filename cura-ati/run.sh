#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

USER_ID=$(id -u)
GROUP_ID=$(id -g)

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "Outputting result to: '$OUTPUT_DIR'\n" \
        "" | $BOXES -d columns

docker build --tag=$IMAGE_NAME .
if [ $? -eq 0 ]; then
    echo -e "\n" \
            "Image built succesfully!\n" \
            "Starting the program\n" \
            "" | $BOXES -d stone
    docker run -it --rm -v /:/host/ -e uid=$USER_ID -e gid=$GROUP_ID -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix $IMAGE_NAME
else
    echo -e "\n" \
            "Error: Image building failed.\n" \
            " - please check build log and try again.\n" \
            "" | $BOXES -d stone 1>&2
fi

