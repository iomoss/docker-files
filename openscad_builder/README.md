# Openscad_builder

Repository for building `openscad` for debian:jessie.

## Building / running the image
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/openscad_builder
$ ./run.sh /home/skeen/openscad
```
Or stand-alone;
```bash
$ export OUTPUT_DIR=/home/skeen/openscad
$ docker run -v $OUTPUT_DIR:/srv/ -it iomoss/openscad_builder
```

This will;

1. Setup the building environment in a docker image.
2. Run the docker image, and start compiling the openscad source code.
3. The openscad deb files will be output to `$OUTPUT_DIR`

Warning: Compiling of openscad is multithreaded, and can make the computer irresponsible.
