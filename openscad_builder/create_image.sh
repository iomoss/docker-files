#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "1" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./create_image.sh [[ARCH]]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

ARCH=$1

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR:$ARCH"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "" | $BOXES -d columns

if [ "$ARCH" == "amd64" ]; then
    echo "FROM debian:jessie" | cat - Dockerfile.in > Dockerfile
elif [ "$ARCH" == "i386" ]; then
    echo "FROM 32bit/debian:jessie" | cat - Dockerfile.in > Dockerfile
else
    echo -e "\n" \
            "Error: Unsupported architecture.\n" \
            "Supported architectures are: amd64, i386\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi
docker build --tag=$IMAGE_NAME .

if [ $? -eq 0 ]; then
    echo -e "\n" \
            "Image built succesfully!\n" \
            "" | $BOXES -d stone
else
    echo -e "\n" \
            "Error: Image building failed.\n" \
            " - please check build log and try again.\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi
rm Dockerfile

