# Docker-files

A repository of docker-files used in-house at IOMOSS.
Almost all images are based upon debian:jessie.

Each folder has it's own README.md explaining the purpose and operation of the image.
