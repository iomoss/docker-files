# Backup (rsnapshot)

Hourly (24), daily (7), weekly (4), monthly (12) remote (ssh) backup.

## Building the image (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/backup
$ ./create-image.sh
```
Access to the repository requires affiliation with IOMOSS.

## Running (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/backup
$ ./run.sh /home/skeen/backup/conf /home/skeen/backup/data
```
Access to the repository requires affiliation with IOMOSS.

## Running (stand-alone)
Assuming the configuration files are made, you can start the server as;
```bash
$ export CONFIG_FOLDER=/home/config_here
$ export BACKUP_FOLDER=/home/data_here
$ docker run -v $CONFIG_FOLDER:/srv/ -v $BACKUP_FOLDER:/backup/ -d iomoss/backup
```

## Configurating
### Setting up SSH-keys
The keys are required for pulling in data to the system, and should be added to;
```bash
$CONFIG_FOLDER/root/.ssh/id_rsa
```
Assuming you have generated a ssh key-set on the machine, you can do this by running;
```bash
$ export CONFIG_FOLDER=/home/config_here
$ cp ~/.ssh/id_rsa $CONFIG_FOLDER/root/.ssh/id_rsa
```
Generating a ssh key-set can be done by running;
```bash
$ ssh-keygen
```
And following the instructions.

Make sure this key is installed to the remote hosts `authorized_keys` file.
This can be done by running;
```bash
$ ssh-copy-id USERNAME@HOSTNAME
```
`USERNAME` and `HOSTNAME` should correspond to the ones in the backup-targets file.

### Setting up `backup-targets`
The backup targets file is used to decide what to backup, and should be added to;
```bash
$CONFIG_FOLDER/root/backup-targets
```
It's formatted as the backup targets in `rsnapshot.conf`;
```bash
# Example Entry
backup  USERNAME@HOSTNAME:/home/skeen/www/ skeen-website/
```
*Note: The ssh private key from above should be in the `authorized_keys` file at
`/home/$USERNAME/.ssh/authorized_keys` on the `HOSTNAME`.*
