#!/bin/bash

# SSH authorized keys
echo "---SSH-KEYS---"
SSH_PRIVATE_KEY=/root/.ssh/id_rsa
SSH_PRIVATE_KEY_MOUNT=$(echo "/srv$SSH_PRIVATE_KEY")
if [ ! -f $SSH_PRIVATE_KEY_MOUNT ]; then
    SSH_KEY_DIR=$(dirname $SSH_PRIVATE_KEY_MOUNT)
    echo "Error: No ssh-private key found!"
    echo " Please provide one to: \$CONFIG_DIR$SSH_PRIVATE_KEY"
    mkdir -p $SSH_KEY_DIR
    echo ""
    exit -1
else
    PASSWORD_KEY=$(grep -L ENCRYPTED $SSH_PRIVATE_KEY_MOUNT)
    if [[ -z "$PASSWORD_KEY" ]]; then
        echo "Error: Invalid key (it's password proctected)!"
        exit -1
    fi
    echo "Installing ssh-keys...    OK"
    cp $SSH_PRIVATE_KEY_MOUNT $SSH_PRIVATE_KEY
fi

# Backup targets
echo "---BACKUP-TARGETS---"
BACKUP_TARGETS=/root/backup-targets
BACKUP_TARGETS_MOUNT=$(echo "/srv$BACKUP_TARGETS")
if [ ! -f $BACKUP_TARGETS_MOUNT ]; then
    echo "Error: No backup-targets found!"
    echo " Please provide one to: \$CONFIG_DIR$BACKUP_TARGETS"
    echo ""
    exit -1
else
    echo "Finding backup targets... OK"
    cp $BACKUP_TARGETS_MOUNT $BACKUP_TARGETS
fi

# Setup rsnapshot.conf
echo "---SNAPSHOT.CONF---"
cat /templates/rsnapshot.conf > /etc/rsnapshot.conf
cat /root/backup-targets >> /etc/rsnapshot.conf

# Test rsnapshot configuration
echo -e "Testing rsnapshot.conf... \c"
CONFIG_TEST=$(rsnapshot configtest)
if [ "$CONFIG_TEST" == "Syntax OK" ]; then
    echo "OK"
else
    exit -1
fi

# Cron and output
echo "---CRON & OUTPUT---"
echo -e "Running.\c"
# Create the backup log folder
mkdir -p /backup/log/

# Create the backup log file
rm -f /backup/log/rsnapshot.log
touch /backup/log/rsnapshot.log
echo -e ".\c"

# Start up cron and tail the log
cron
echo "."
tail -f /backup/log/rsnapshot.log
