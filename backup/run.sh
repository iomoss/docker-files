#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "2" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./run.sh [CONFIGURATION_FOLDER] [BACKUP_FOLDER]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

CONFIG_FOLDER=$1
BACKUP_FOLDER=$2

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "Configuration from: '$CONFIG_FOLDER'\n" \
        "Backup output to: '$BACKUP_FOLDER'\n" \
        "" | $BOXES -d columns

echo "Image built succesfully; starting the build command!"
docker run -v $CONFIG_FOLDER:/srv/ -v $BACKUP_FOLDER:/backup/ -it $IMAGE_NAME
