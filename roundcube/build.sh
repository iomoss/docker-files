#!/bin/bash

ROUNDCUBE_CONF=/etc/roundcube/config.inc.php

function test_set {
    KEY=$1
    VAL=$(echo ${!KEY})
    if [[ -z "$VAL" ]]; then
        echo "Error: No \$$KEY set!"
        exit -1
    fi
}

test_set DEFAULT_HOST
test_set SMTP_SERVER
test_set SMTP_PORT

# TODO: Cleanup the tempalate, it ignores all ssl errors currently!!
cat /templates/config.inc.php |\
sed "s#!!!DEFAULT_HOST_HERE!!!#$DEFAULT_HOST#g" |\
sed "s#!!!SMTP_SERVER_HERE!!!#$SMTP_SERVER#g" |\
sed "s#!!!SMTP_PORT_HERE!!!#$SMTP_PORT#g" \
> $ROUNDCUBE_CONF

apache2ctl -D FOREGROUND -k start
