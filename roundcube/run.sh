#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "4" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./run.sh [DEFAULT_HOST] [SMTP_SERVER] [SMTP_PORT] [WEBSERVER_PORT]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

DEFAULT_HOST=$1
SMTP_SERVER=$2
SMTP_PORT=$3
WEBSERVER_PORT=$4

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "Default host (imap): '$DEFAULT_HOST'\n" \
        "SMTP Server host: '$SMTP_SERVER'\n" \
        "SMTP Server port: '$SMTP_PORT'\n" \
        "Webserver mapped to: '$WEBSERVER_PORT'\n" \
        "" | $BOXES -d columns

echo "Image built succesfully; starting the build command!"
docker run -p $WEBSERVER_PORT:$WEBSERVER_PORT \
           -e DEFAULT_HOST=$DEFAULT_HOST \
           -e SMTP_SERVER=$SMTP_SERVER \
           -e SMTP_PORT=$SMTP_PORT \
           -it $IMAGE_NAME
