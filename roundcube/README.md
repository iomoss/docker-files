# Roundcube (WebMail)

* Based upon: [the guide for setting up a roundcube webmail](http://www.cpierce.org/2012/04/roundcube-for-your-debian-squeeze-mail-server/).
* Purpose: Webmail with sqllite backend and external imap/smtp server.

## Building the image (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/roundcube
$ ./create-image.sh
```
Access to the repository requires affiliation with IOMOSS.

## Running (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/roundcube
$ ./run.sh ssl://EXTERNAL_URL EXTERNAL_URL 25 80
```
Access to the repository requires affiliation with IOMOSS.

## Running (stand-alone)

### Configurating
The image is configurated using environmental variables;

```bash
$ export HOSTNAME='{{YOUR-DOMAIN-NAME}}'
$ export DEFAULT_HOST='ssl://$HOSTNAME'
$ export SMTP_SERVER='$HOSTNAME'
$ export SMTP_PORT=25
$ export WEBSERVER_PORT=80
$ docker run -p $WEBSERVER_PORT:$WEBSERVER_PORT \
             -e DEFAULT_HOST=$DEFAULT_HOST \
             -e SMTP_SERVER=$SMTP_SERVER \
             -e SMTP_PORT=$SMTP_PORT \
             -it iomoss/roundcube
```

### Configuration variables
*Note: Configuration is rewritten on each bootup*

* `$HOSTNAME`: The hostname of the IMAP/SMTP
* `$DEFAULT_HOST`: The ssl name of the IMAP server
* `$SMTP_SERVER`: The hostname of the SMTP server
* `$SMTP_PORT`: The access port of the SMTP server
* `$WEBSERVER_PORT`: The exposed apache port (where the webmail is exposed).

