# Kicad_builder

Repository for building `kicad-4.*` for debian:jessie.

## Building / running the image
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/kicad_builder
$ ./run.sh /home/skeen/kicad
```
Or stand-alone;
```bash
$ export OUTPUT_DIR=/home/skeen/kicad
$ docker run -v $OUTPUT_DIR:/srv/ -it iomoss/kicad_builder
```

This will;

1. Setup the building environment in a docker image.
2. Run the docker image, and start compiling the kicad source code.
3. The kicad deb files will be output to `$OUTPUT_DIR`

Warning: Compiling of kicad is multithreaded, and can make the computer irresponsible.
