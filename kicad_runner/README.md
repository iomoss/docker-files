# Kicad_runner

Repository for running `kicad-4.*` on any linux.

## Building / running the image
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/kicad_runner
$ ./run.sh
```
Or stand-alone;
```bash
$ docker run -it --rm -v /:/host/ -e uid=$(id -u) -e gid=$(id -g) -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix iomoss/kicad_runner
```

This will;

1. Install the program in a docker image.
2. Run the docker image, and start the kicad program on hosts x-server.
3. The host system is mounted under /host/
