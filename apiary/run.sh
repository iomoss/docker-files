#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "2" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./run.sh [API_FOLDER] [WEBSERVER_PORT]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

API_FOLDER=$1
WEBSERVER_PORT=$2

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "API from: '$API_FOLDER'\n" \
        "Webserver mapped to: '$WEBSERVER_PORT'\n" \
        "" | $BOXES -d columns

echo "Image built succesfully; starting the build command!"
docker run -v $API_FOLDER:/usr/src/api -p $WEBSERVER_PORT:8080 -it $IMAGE_NAME
