Apiary CLI Client `preview --server`
====================================
[Apiary](https://apiary.io) CLI client, `apiary`.

## Description

The Apiary CLI Client is a command line tool for developing and previewing
[API Blueprint](http://apiblueprint.org) documents locally. 

Github: https://github.com/apiaryio/apiary-client

## About this image

This image runs `apiary preview --server` inside `/usr/src/app`,
thereby serving the documentation on port `8080`.

This allows developers to use apiary; preview and develop their
documentation, without having to install apiary themselves.

## Building the image (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/apiary
$ ./create-image.sh
```
Access to the repository requires affiliation with IOMOSS.

## Running (in-house)
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/apiary
$ ./run.sh /home/skeen/api-folder 3000
```
Access to the repository requires affiliation with IOMOSS.

## Running (stand-alone)
Assuming the configuration is done, you can start the server as;
```bash
$ export API_FOLDER=/home/api-folder/
$ export WEBSERVER_PORT=8080
$ docker run -v $API_FOLDER:/usr/src/api -p $WEBSERVER_PORT:8080 -d iomoss/apiary
```

### Configurating

* `$API_FOLDER`: The folder in which the apiary.apib file is stored.
* `$WEBSERVER_PORT`: The exposed apiary preview server port (where API preview is served).
