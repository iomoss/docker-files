#!/bin/bash

echo "Attempting to backport package:"
echo "$@"

cd /root/
dget -x $@

cd $(ls -d */|head -n 1)
mk-build-deps -i -t "apt-get -y"

dch --local ~jbd --distribution jessie-backports "Backported by jessie-backporter-docker (jbd) for jessie."
dpkg-buildpackage -us -uc -b

/bin/bash
