#!/bin/bash

# Create our home directory
mkdir -p /home/developer
# Setup our user
echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd
# Setup our groups
echo "developer:x:${uid}:" >> /etc/group
# No password sudo access
echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer
# Set permissions on sudoers file
chmod 0440 /etc/sudoers.d/developer
# Set ownership of home folder
chown ${uid}:${gid} -R /home/developer

# Setup user and launch program
sudo -u developer HOME=/home/developer libreoffice
