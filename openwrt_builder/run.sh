#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "1" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./run.sh [ABSOLUTE_OUTPUT_DIRECTORY]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

OUTPUT_DIR=$1

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "Outputting result to: '$OUTPUT_DIR'\n" \
        "" | $BOXES -d columns

docker build --tag=$IMAGE_NAME .
mkdir -p $OUTPUT_DIR
if [ $? -eq 0 ]; then
    echo -e "\n" \
            "Image built succesfully!\n" \
            "Starting the build command\n" \
            "" | $BOXES -d stone
    docker run -v $OUTPUT_DIR:/srv -it $IMAGE_NAME
else
    echo -e "\n" \
            "Error: Image building failed.\n" \
            " - please check build log and try again.\n" \
            "" | $BOXES -d stone 1>&2
fi
