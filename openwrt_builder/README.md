# Openwrt_builder

Repository for building openwrt firmware images.

## Building / running the image
```bash
$ git clone git@bitbucket.org:iomoss/docker-files.git
$ cd docker-files/openwrt_builder
$ ./run.sh /home/skeen/firmware
```
Or stand-alone;
```bash
$ export OUTPUT_DIR=/home/skeen/firmware
$ docker run -v $OUTPUT_DIR:/srv/ -it iomoss/openwrt_builder
```

This will;

1. Setup the building environment in a docker image.
2. Run the docker image, open up the visual configuration environment.
3. Start compiling the openwrt source source code.
3. The openwrt firmwire files will be output to `$OUTPUT_DIR`

Warning: Compiling of openwrt is multithreaded, and can make the computer irresponsible.
