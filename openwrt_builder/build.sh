#!/bin/bash

NUMBER_OF_CORES=$(nproc)

# Configuration
make menuconfig
# Compilation
nice -n 19 make -j$NUMBER_OF_CORES
# Output
mv /home/openwrt/bin/* /srv/
chmod -R 666 /srv/

